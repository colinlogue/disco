import Distribution.Simple

import Apiary (renderApiFile)
import System.Directory
import System.Process

elmMake :: String -> String -> String -> IO ()
elmMake dir files output = do
  createDirectoryIfMissing True "elm/dest"
  cmd <- pure $
    "cd "++dir++"; elm make "++files ++" --output=" ++ output
  createProcess $ shell cmd
  pure ()

main = do
  createDirectoryIfMissing True "apiary/dest"
  renderApiFile "apiary/api" "apiary/dest/API.hs"
  elmMake "elm" "src/Main.elm" "dest/page.html"
  defaultMain

{-# LANGUAGE PatternSynonyms #-}

module Result
  ( Result
  , pattern Ok
  , pattern Err
  ) where

type Result = Either
pattern Ok x = Right x
pattern Err x = Left x
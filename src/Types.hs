module Types where

import Result

type LessonID = Int
type ExerciseID = Int
type StudentID = Int

type ParseResult = Result ParseError
type ParseError = [String]
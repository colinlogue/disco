{-# LANGUAGE DeriveGeneric #-}

module Lesson where

import Data.Aeson
import GHC.Generics


data Lesson = Lesson
  { title :: String
  , description :: String
  , exercises :: [Exercise]
  } deriving (Generic)

instance ToJSON Lesson

data Exercise = Exercise
  { text :: String
  } deriving (Generic)

instance ToJSON Exercise


lesFileById :: Int -> FilePath
lesFileById x = "data/lesson" ++ show x ++ ".les"

readLessonFile :: FilePath -> IO Lesson
readLessonFile fp = readFile fp >>= pure . parseLessonFile

parseLessonFile :: String -> Lesson
parseLessonFile s = Lesson
  { title = "Lesson Title"
  , description = s
  , exercises = []
  }
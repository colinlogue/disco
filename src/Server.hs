module Server
  ( runserver
  ) where


import Apiary.Handle (handle)
import Apiary.Serve
import Apiary.Types
import Data.Aeson
import Data.ByteString.Builder
  (Builder, stringUtf8, lazyByteString)
import Network.HTTP.Types
import Network.Wai
import Network.Wai.Handler.Warp
import Network.Wai.Helpers
import System.IO.Error (tryIOError)


import API (Action(..), handlers)
import Lesson



instance Servable Action where
  -- invalid :: Action
  invalid = InvalidAction

  --serve :: Action -> IO Response
  serve action = case action of
    ServePage ->
      readFile "elm/dest/page.html" >>= pure . (okResponse [])
    GetLessons ->
      pure $ okResponse [] $ "Lessons"
    GetLesson x ->
      (do
        result <- tryIOError $ readLessonFile $ lesFileById x
        pure $ either
          (\e -> errNotFound)
          (\les -> jsonResponse [] les)
          result)
    GetExercise x y ->
      pure $ okResponse [] $ "Exercise " ++ show (x,y)
    GetAnswers x y ->
      pure $ okResponse [] $ "Answers " ++ show (x,y)
    GetAnswer x y z ->
      pure $ okResponse [] $ "Answer " ++ show (x,y,z)
    PutAnswer x y z s ->
      pure $ okResponse [] $ "Put answer " ++ show (x,y,z,s)
    InvalidAction -> pure errNotFound

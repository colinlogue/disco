module Main where

import Apiary (renderApiFile)
import Apiary.Serve
import System.Environment

import API (handlers)
import Server

main :: IO ()
main = runserver 8080 handlers

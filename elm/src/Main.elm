module Main exposing (main)

import Browser exposing (Document)
import Cmd.Extra exposing (..)
import Html exposing (Html, div, h1, p, text)
import Html.Attributes exposing (class, id)
import Http
import Json.Decode exposing (Value)
import Platform.Cmd exposing (Cmd)
import String exposing (..)
import Tuple exposing (..)
import Url.Builder exposing (absolute)

import Lesson exposing (Lesson, Exercise)

type alias ErrorMsg = String
type alias Flags = Json.Decode.Value

type Model
  = LessonPage Lesson
  | LoadFailure ErrorMsg
  | PreloadPage

type Msg
  = GotLesson Lesson
  | LoadData (Result Http.Error String)

body : Html msg -> List (Html msg)
body content = [ div [ id "container" ] [content] ]

layout : List (Html msg) -> Html msg
layout stuff = div [ id "lessons" ] stuff

load : Lesson.LessonId -> Cmd Msg
load les_id =
  Http.get
    { url = absolute [ "lessons", fromInt les_id ] []
    , expect = Http.expectString LoadData
    }

-- parse a loaded lesson string
parseLesson : String -> Maybe Lesson
parseLesson str =
  Just
    { title = "Lesson"
    , description = str
    , language = "Haskell"
    , exercises = []
    }

init : Flags -> (Model, Cmd Msg)
init _ = PreloadPage |> withCmd (load 1)

update : Msg -> Model -> (Model, Cmd Msg)
update msg mod =
  case msg of
    GotLesson les -> LessonPage les |> withNoCmd
    LoadData (Ok str) -> case parseLesson str of
      Just les -> LessonPage les |> withNoCmd
      Nothing -> LoadFailure ("Parse failure: " ++ str)
        |> withNoCmd
    LoadData (Err e) -> LoadFailure "http error" |> withNoCmd

view : Model -> Document Msg
view mod =
  let
    stuff =
      case mod of
        LessonPage les -> Lesson.render les
        LoadFailure err -> [text err]
        PreloadPage -> [text "loading"]
  in
    { title = "Title"
    , body = body <| layout stuff
    }

subscriptions : Model -> Sub Msg
subscriptions _ = Sub.none

main : Program Flags Model Msg
main = Browser.document
  { init = init
  , view = view
  , update = update
  , subscriptions = subscriptions
  } 
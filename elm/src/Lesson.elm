module Lesson exposing (Lesson, Exercise, LessonId, ExerciseId, render)

import Html exposing (Html, div, text)
import Html.Attributes exposing (class)
import Http
import String exposing (fromInt)
import Url.Builder exposing (absolute)


type alias Language = String

type alias Lesson =
  { title : String
  , description : String
  , language : Language
  , exercises : List Exercise
  }

type alias Exercise =
  { text : String
  }

type alias LessonId = Int
type alias ExerciseId = Int


---- renders a lesson to HTML
render : Lesson -> List (Html msg)
render les =  [ div [ class "lesson" ] [ text les.title ] ]

--getLesson : LessonId -> Result String Lesson
--getLesson les_id =
--  case load les_id of
--    GotLesson (Ok str) ->
--      Ok
--        { title = "title"
--        , description = str
--        , language = "haskell"
--        , exercises = []
--        }
--    GotLesson (Err str) ->
--      Err "failed to load lesson"